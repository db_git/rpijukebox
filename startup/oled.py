import board
import digitalio
import adafruit_ssd1306
from sys import argv
from PIL import Image, ImageDraw, ImageFont

oled = adafruit_ssd1306.SSD1306_SPI(128, 64, board.SPI(),
				digitalio.DigitalInOut(board.D6),
				digitalio.DigitalInOut(board.D4),
				digitalio.DigitalInOut(board.D5))
oled.fill(0)
oled.show()

img = Image.new("1", (oled.width, oled.height))
font = ImageFont.load_default()
draw = ImageDraw.Draw(img)
argn = len(argv)

(fw1, fh1) = font.getsize(argv[1])
if argn == 2:
	(fw2, fh2) = font.getsize(argv[2])
if argn == 3:
	(fw2, fh2) = font.getsize(argv[2])
	(fw3, fh3) = font.getsize(argv[3])

draw.text((oled.width // 2 - fw1 // 2, oled.height // 2 - fh1 - 6), argv[1], font=font, fill=255)
if argn == 2:
	draw.text((oled.width // 2 - fw2 // 2, oled.height // 2 - fh2 + 10 ), argv[2], font=font, fill=255)
if argn == 3:
	draw.text((oled.width // 2 - fw2 // 2, oled.height // 2 - fh2 + 10 ), argv[2], font=font, fill=255)
	draw.text((oled.width // 2 - fw3 // 2, oled.height // 2 - fh3 + 26 ), argv[3], font=font, fill=255)

oled.image(img)
oled.show()
