from time import sleep
from os import system
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(3, GPIO.IN, pull_up_down=GPIO.PUD_UP)

while 1:
	try:
		sleep(1.5)
		GPIO.wait_for_edge(3, GPIO.FALLING)
		system("sudo -i -u admin sudo /sbin/init 0")
	except:
		pass

GPIO.cleanup()
