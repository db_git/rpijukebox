from sys import exit, argv

arg_num = len(argv)
if arg_num < 2 or arg_num == 4 or arg_num > 5:
	exit(0)

try:
	file = open("/mpd/php/status.txt", "r")
	if (str(file.read()) == str(argv[1])):
		file.close()
		exit(0)
except IOError:
	file = ""

file = open("/mpd/php/status.txt", "w")
file.write(str(argv[1]))
file.close()

from PIL import Image, ImageDraw, ImageFont
import adafruit_ssd1306

registry = {}
class function_overload(object):
	def __init__(self, name):
		self.name = name
		self.typemap = {}
	def __call__(self, *args):
		types = tuple(arg.__class__ for arg in args)
		function = self.typemap.get(types)
		return function(*args)
	def register(self, types, function):
		self.typemap[types] = function

def overload(*types):
	def register(function):
		name = function.__name__
		mm = registry.get(name)
		if mm is None:
			mm = registry[name] = function_overload(name)
		mm.register(types, function)
		return mm
	return register

@overload(str, ImageFont.ImageFont, adafruit_ssd1306.SSD1306_SPI)
def show(text1, font, oled):
	(fw1, fh1) = font.getsize(text1)

	draw.text(
		(oled.width // 2 - fw1 // 2, oled.height // 2 - fh1),
		text1, font=font, fill=255)

@overload(str, str, ImageFont.ImageFont, adafruit_ssd1306.SSD1306_SPI)
def show(text1, text2, font, oled):
	(fw1, fh1) = font.getsize(text1)
	(fw2, fh2) = font.getsize(text2)

	draw.text(
		(oled.width // 2 - fw1 // 2, oled.height // 2 - fh1 - 16),
		text1, font=font, fill=255)
	draw.text(
		(oled.width // 2 - fw2 // 2, oled.height // 2 - fh2 + 16),
		text2, font=font, fill=255)

@overload(str, str, str, str, ImageFont.ImageFont, adafruit_ssd1306.SSD1306_SPI)
def show(text1, text2, text3, text4, font, oled):
	(fw1, fh1) = font.getsize(text1)
	(fw2, fh2) = font.getsize(text2)
	(fw3, fh3) = font.getsize(text3)
	(fw4, fh4) = font.getsize(text4)

	draw.text(
		(oled.width // 2 - fw1 // 2, oled.height // 5 - fh1),
		text1, font=font, fill=255)
	draw.text(
		(oled.width // 2 - fw2 // 2, oled.height // 2 - fh2 - 3),
		text2, font=font, fill=255)
	draw.text(
		(oled.width // 2 - fw3 // 2, oled.height // 5 - fh3 + 35),
		text3, font=font, fill=255)
	draw.text(
		(oled.width // 2 - fw4 // 2, oled.height // 2 - fh4 + 32),
		text4, font=font, fill=255)

import board
import digitalio

ONE_STR_ARG = 2
TWO_STR_ARG = 3
FOUR_STR_ARG = 5

SPI = board.SPI()
SPI_RS = digitalio.DigitalInOut(board.D4)
SPI_CS = digitalio.DigitalInOut(board.D5)
SPI_DC = digitalio.DigitalInOut(board.D6)

oled = adafruit_ssd1306.SSD1306_SPI(128, 64, SPI, SPI_DC, SPI_RS, SPI_CS)
oled.fill(0)
oled.show()

img = Image.new("1", (oled.width, oled.height))
font = ImageFont.load_default()
draw = ImageDraw.Draw(img)

if arg_num == ONE_STR_ARG:
	show(
		(argv[1][:18] + ".." if len(argv[1]) > 20 else argv[1]),
		font, oled)

elif arg_num == TWO_STR_ARG:
	show(
		(argv[1][:18] + ".." if len(argv[1]) > 20 else argv[1]),
		(argv[2][:18] + ".." if len(argv[2]) > 20 else argv[2]),
		font, oled)

elif arg_num == FOUR_STR_ARG:
	show(
		(argv[1][:18] + ".." if len(argv[1]) > 20 else argv[1]),
		(argv[2][:18] + ".." if len(argv[2]) > 20 else argv[2]),
		(argv[3][:18] + ".." if len(argv[3]) > 20 else argv[3]),
		(argv[4][:18] + ".." if len(argv[4]) > 20 else argv[4]),
		font, oled)

oled.image(img)
oled.show()
