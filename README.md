## **Projekt iz kolegija Programiranje Malih Linux Računala**

### _Raspberry Pi Jukebox_

```
git clone https://gitlab.com/db_git/rpijukebox
```

#### Sve potrebno nalazi se u datoteci [dokumentacija/dokumentacija.tex](https://gitlab.com/db_git/rpijukebox/-/blob/master/dokumentacija/dokumentacija.tex)
![Izgled sučelja na mobitelu](slika.jpg)
