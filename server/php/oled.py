from sys import exit, argv

try:
	file = open("/mpd/php/status.txt", "r")
	if (str(file.read()) == str(argv[1])):
		file.close()
		exit(0)
except IOError:
	file = ""

file = open("/mpd/php/status.txt", "w")
file.write(str(argv[1]))
file.close()

import board
import digitalio
import adafruit_ssd1306
from PIL import Image, ImageDraw, ImageFont

oled = adafruit_ssd1306.SSD1306_SPI(128, 64, board.SPI(),
				digitalio.DigitalInOut(board.D6),
				digitalio.DigitalInOut(board.D4),
				digitalio.DigitalInOut(board.D5))
oled.fill(0)
oled.show()

img = Image.new("1", (oled.width, oled.height))
font = ImageFont.load_default()
draw = ImageDraw.Draw(img)

text1 = ((argv[1][:18] + ".." if len(argv[1]) > 20 else argv[1]))
text2 = ((argv[2][:18] + ".." if len(argv[2]) > 20 else argv[2]))
text3 = ((argv[3][:18] + ".." if len(argv[3]) > 20 else argv[3]))
text4 = ((argv[4][:18] + ".." if len(argv[4]) > 20 else argv[4]))

(fw1, fh1) = font.getsize(text1)
(fw2, fh2) = font.getsize(text2)
(fw3, fh3) = font.getsize(text3)
(fw4, fh4) = font.getsize(text4)

draw.text((oled.width // 2 - fw1 // 2, oled.height // 5 - fh1), text1, font=font, fill=255)
draw.text((oled.width // 2 - fw2 // 2, oled.height // 2 - fh2 - 3), text2, font=font, fill=255)
draw.text((oled.width // 2 - fw3 // 2, oled.height // 5 - fh3 + 35), text3, font=font, fill=255)
draw.text((oled.width // 2 - fw4 // 2, oled.height // 2 - fh4 + 32), text4, font=font, fill=255)

oled.image(img)
oled.show()
