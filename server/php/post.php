<?php

function listArtists($mpd) {
	echo "<script>document.getElementById('files').innerHTML = \"\";</script>";
	echo "<form class='file_form' method='post'>";

	$artists = array_diff(scandir("music/"), array('.', '..', '.keep'));
	foreach ($artists as $key => $artist) {
		if (!strncmp($artist, "upload", strlen("upload"))) {
			echo "<li><input class='file_input' type='submit' name='upload[]' value=\"$artist\"/>";
		} else {
			echo "<li><input class='file_input' type='submit' name='artist[]' value=\"$artist\"/>";
		}
		echo "<button class='file_add' type='button' onclick='add(\"$artist\");reload();'>
					<img class=\"file_add\" src=\"icon/all.webp\"></button></li>";
	}
	echo "</form>";
}

function listAlbums($artist, $mpd) {
	$albums = $mpd->listDir($artist);

	echo "<script>document.getElementById('files').innerHTML = \"\";</script>";
	echo "<form class='file_form' method='post'>";
	echo "<input class='file_input' type='submit' name='back' value=\"..\"/>";

	for ($i = 0; $i < count($albums); $i++) {
		$album = $albums[$i]['name'];
		echo "<input class='file_input' type='submit' name='album[]' value=\"$album\"/>
						<button class='file_add' type='button' onclick='add(\"$album\");reload();'>
						<img class=\"file_add\" src=\"icon/all.webp\"></button></li>";
	}
	echo "</form>";
}

function listUpload($mpd) {
	echo "<script>document.getElementById('files').innerHTML = \"\";</script>";
	echo "<form class='file_form' method='post'>";
	$songs = $mpd->listDir("upload");
	echo "<input class='file_input' type='submit' name='back' value=\"..\"/>";
	for ($i = 0; $i < count($songs); $i++) {
		$song = $songs[$i]['name'];
		echo "<input class='file_input' type='submit' name='song[]' value=\"$song\"/>";
	}
	echo "</form>";
}

function listSongs($album, $mpd) {
	$songs = $mpd->listDir($album);
	echo "<script>document.getElementById('files').innerHTML = \"\";</script>";
	echo "<form class='file_form' method='post'>";
	echo "<input class='file_input' type='submit' name='back' value=\"..\"/>";

	for ($i = 0; $i < count($songs); $i++) {
		$song = $songs[$i]['name'];
		echo "<input class='file_input' type='submit' name='song[]' value=\"$song\"/>";
	}
	echo "</form>";
}

function listPlaylist($mpd) {
	$current = $mpd->playlist();
	if ($current) {
		echo "<script>document.getElementById('queue').innerHTML = \"\";</script>";
		echo "<form id='form_queue' method='post'>";
		for ($i = 0; $i < count($current); $i++) {
			$song_id = $current[$i]['Id'];
			echo "<li>";
			if ($current[$i]['Id'] == $mpd->getCurrentSongId())
				echo "<input style='color: #0047ab;' class='queue_input'
							type='button' name='playing' value=\"";
			else echo "<input class='queue_input'
							type='button' name='playing' value=\"";

			echo isset($current[$i]['Artist']) ? $current[$i]['Artist']." - " : "Unknown";
			echo isset($current[$i]['Title']) ? $current[$i]['Title'] : " - Unknown";
			echo "\" onclick=\"playid($song_id);reload();reload_queue();reload_btn();\"/>";
			echo "<button class=\"queue_remove\" type=\"button\"
						onclick=\"removeid($song_id);reload();reload_queue();reload_btn();\">
						<img class=\"queue_remove\" src=\"icon/remove.webp\"></button></li>";
		}
		echo "</form>";
		echo "<br>";
	}
}
