<?php

/* 
 * This is a slightly modified version from
 * https://github.com/jimmikristensen/PHP-MPD
 */

class MPD {
	private $host			= 'localhost';
	private $port			= 6600;
	private $sock			= NULL;
	private $is_connected		= false;
	private $music_dir		= "music/";
	private $version;
	private $playlist;
	private $server_status;
	private $server_statistics;
	private $state			= "stop";
	private $current_track_id;
	private $current_track_pos;
	private $current_track_len;
	private $volume;
	private $db_last_updated	= 0;
	private $uptime;
	private $playtime;
	private $num_artists;
	private $num_songs;
	private $num_albums;

	function __construct() {
		if (!$this->is_connected)
			$this->connect();
	}

	function play($song_pos) {
		$this->sendCommand("play", array($song_pos));
		$this->update();
	}

	function playid($song_id) {
		$this->sendCommand("playid", array($song_id));
		$this->update();
	}

	function pause() {
		$this->sendCommand("pause");
		$this->update();
	}

	function stop() {
		$this->sendCommand("stop");
		$this->update();
	}

	function next() {
		$this->sendCommand("next");
		$this->update();
	}

	function prev() {
		$this->sendCommand("previous");
		$this->update();
	}

	function seek($time) {
		$this->update();

		if ($time < 0)
			if ($time + $this->current_track_pos < 0)
				$this->prev();
		else
			if ($time + $this->current_track_pos > $this->current_track_len)
				$this->next();

		$this->sendCommand("seekcur", array($time));
	}

	function setvol($vol) {
		$this->update();
		$vol = $this->volume + $vol;
		if ($vol <= 0)
			$vol = 0;
		elseif ($vol >= 100)
			$vol = 100;
		$this->sendCommand("setvol", array($vol));
		$this->volume = $vol;
	}

	function updateDb($uri = '') {
		$update_res = $this->sendCommand("update", array($uri));
		if ($update_res !== false) {
			$this->update();
			return $this->parseList($update_res, true);
		}
	}

	function playlist() {
		$this->update();
		return $this->playlist;
	}

	function playlistAdd($uri) {
		$this->sendCommand("add", array($uri));
		$this->update();
	}

	function playlistRemove($id) {
		$this->sendCommand("deleteid", array($id));
		$this->update();
	}

	function playlistClear() {
		$this->sendCommand("clear");
		$this->update();
	}

	function listDir($uri = '') {
		$dir_list = $this->sendCommand("lsinfo", array($uri));
		return $this->parsePlaylist($dir_list);
	}

	function getState() {
		return $this->state;
	}

	function getConnectionStatus() {
		return $this->is_connected;
	}

	function getVersion() {
		return $this->version;
	}

	function getCurrentSongId() {
		return $this->current_track_id;
	}

	function getCurrentSongTime() {
		return $this->current_track_pos;
	}

	function getCurrentSongLength() {
		return $this->current_track_len;
	}

	function getCurrentSongInfo() {
		$this->update();
		$cur_song_res = $this->sendCommand("currentsong");

		$playing = $this->current_track_id;
		if ($playing == -1) return NULL;
		$info = $this->parsePlaylist($cur_song_res);

		isset($info[0]['Artist']) ? $artist = $info[0]['Artist'] : $artist = "Unknown";
		isset($info[0]['Album']) ? $album = $info[0]['Album'] : $album = "Unknown";
		isset($info[0]['Title']) ? $title = $info[0]['Title'] : $title = "Unknown";
		isset($info[0]['Date']) ? $year = $info[0]['Date'] : $year = "Unknown";

		$art = $this->music_dir.strrev(strstr(strrev($info[0]['name']), "/")) . "folder.jpg";
		if (strpos($art, "upload"))
			if (!strncmp($album, "Unknown", strlen("Unknown"))) $art = "icon/generic.png";
			else $art = $this->music_dir . "upload/" . $artist . " - " . $album . ".jpg";
		if (!file_exists($art)) $art = "icon/generic.png";

		$length_minute = $this->convertToMinutes($this->current_track_len);
		$length_second = $this->convertToSeconds($this->current_track_len);
		$current_minute = $this->convertToMinutes($this->current_track_pos);
		$current_second = $this->convertToSeconds($this->current_track_pos);

		return array(
			'Artwork' => $art,
			'Artist' => $artist,
			'Album' => $album,
			'Name' => $title,
			'Year' => $year,
			'CurrentlyPlaying' => $playing,
			'MinLen' => $length_minute,
			'SecLen' => $length_second,
			'MinCurr' => $current_minute,
			'SecCurr' => $current_second
		);
	}

	function hasDb() {
		return $this->db_last_updated > 1;
	}

	private function convertToMinutes($duration) {
		return (int) ($duration / 60);
	}

	private function convertToSeconds($duration) {
		$min_f = $duration / 60;
		$min_i = (int) ($min_f);
		$seconds = (int) round(($min_f - $min_i) * 60);

		if ($seconds < 10)
			return "0".strval($seconds);
		return $seconds;
	}

	private function sendCommand($cmd, $args = array()) {
		$command_str = '';
		$response_str = '';

		foreach($args as $arg)
			$command_str .= ' "'.$arg.'"';

		$command_str = $cmd.$command_str;
		fputs($this->sock, "$command_str\n");

		while (!feof($this->sock)) {
			$res = fgets($this->sock, 1024);

			if (strncmp("OK", $res, strlen("OK")) == 0)
				break;

			if (strncmp("ACK", $res, strlen("ACK")) == 0)
				list ($tmp, $err) = preg_split("ACK" . ' ', $res);

			$response_str .= $res;
		}
		return $response_str;
	}

	private function connect() {
		$this->sock = fsockopen($this->host, $this->port);
		if (!$this->sock) {
			$this->is_connected = false;
			return;
		}

		$res = '';
		while (!feof($this->sock)) {
			$res = fgets($this->sock, 1024);
			if (strncmp("OK", $res, strlen("OK")) == 0)
				break;
		}

		list ($this->version) = sscanf($res, "OK" . " MPD %s\n");
		$this->is_connected = true;
	}

	private function parseList($list_res, $use_str_assoc = false) {
		$list = array();
		if ($list_res != false) {
			$list_line = strtok($list_res, "\n");
			while ($list_line) {
				list ($key, $value) = preg_split("~: ~", $list_line);
				if ($value != '') {
					if ($use_str_assoc === true)
						$list[$key] = $value;
					else
						$list[] = $value;
				}
				$list_line = strtok("\n");
			}
		}
		return $list;
	}

	private function parsePlaylist($plist_res) {
		$playlist = array();

		if ($plist_res != false) {
			$plist_line = strtok($plist_res, "\n");
			$counter = -1;
			$type = 'unknown';
			while ($plist_line) {
				list ($key, $value) = preg_split("~: ~", $plist_line);
				if ($key == 'file' || $key == 'directory' || $key == 'playlist') {
					$type = $key;
					$counter++;

					$playlist[$counter]['type'] = $type;
					$playlist[$counter]['name'] = $value;
					$playlist[$counter]['basename'] = basename($value);
				} else {
					$playlist[$counter][$key] = $value;
				}

				$plist_line = strtok("\n");
			}
		}
		return $playlist;
	}

	private function update() {
		$srv_stats = array();
		$srv_status = array();

		$stats_res = $this->sendCommand("stats");
		$srv_stats = $this->parseList($stats_res, true);
		$this->server_statistics = $srv_stats;

		$status_res = $this->sendCommand("status");
		$srv_status = $this->parseList($status_res, true);
		$this->server_status = $srv_status;

		$plist_res = $this->sendCommand("playlistinfo");
		$this->playlist = $this->parsePlaylist($plist_res);

		$this->state = $srv_status['state'];
		if ($this->state === "play" || $this->state === "pause") {
			$this->current_track_id = $srv_status['songid'];
			list ($this->current_track_pos, $this->current_track_len) = preg_split("~:~", $srv_status['time']);
		} else {
			$this->current_track_id = -1;
			$this->current_track_pos = -1;
			$this->current_track_len = -1;
		}

		$this->volume = $srv_status['volume'];
		$this->db_last_updated = $srv_stats['db_update'];
		$this->uptime = $srv_stats['uptime'];
		$this->playtime = $srv_stats['playtime'];
		$this->num_artists = $srv_stats['artists'];
		$this->num_songs = $srv_stats['songs'];
		$this->num_albums = $srv_stats['albums'];
	}
}
