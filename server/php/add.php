<?php

require_once("connect.php");

function cleanUp($dirs) {
	$new = array();
	foreach ($dirs as $dir) {
		if (!strncmp(substr($dir, -5), ".keep", 5)) continue;
		elseif (!strncmp(mime_content_type($dir), "image", strlen("image"))) continue;
		else $new[] = substr(strstr($dir, "music/"), strlen("music/"));
	}
	return $new;
}

function getDirContents($dir, $result = array()) {
	$files = scandir($dir);
	foreach ($files as $file) {
		$path = realpath($dir . "/" . $file);
		if (!is_dir($path)) {
			$result[] = $path;
		} else if ($file != "." && $file != "..") {
			getDirContents($path, $result);
			$result[] = $path;
		}
	}
	return $result;
}

$music = cleanUp(getDirContents("../music/" . $_POST['dir']));
foreach ($music as $audio_file) $mpd->playlistAdd($audio_file);
