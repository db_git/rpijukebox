<!DOCTYPE html>
<html lang="hr">

<?php
require_once('php/connect.php');
require_once('php/post.php');
?>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Raspberry Pi Jukebox</title>
	<link rel="icon" type="image/webp" href="icon/fav.webp" sizes="48x48">
	<link rel="stylesheet" href="css/mobile.css">
	<script src="js/jquery.js"></script>
</head>

<body>
	<script src="js/ajax_next.js"></script>
	<script src="js/ajax_pause.js"></script>
	<script src="js/ajax_add.js"></script>
	<script src="js/ajax_play.js"></script>
	<script src="js/ajax_playid.js"></script>
	<script src="js/ajax_removeid.js"></script>
	<script src="js/ajax_prev.js"></script>
	<script src="js/ajax_reset.js"></script>
	<script src="js/ajax_seekb.js"></script>
	<script src="js/ajax_seekf.js"></script>
	<script src="js/ajax_stop.js"></script>
	<script src="js/ajax_update.js"></script>
	<script src="js/ajax_voldown.js"></script>
	<script src="js/ajax_volup.js"></script>
	<script src="js/ajax_oled.js"></script>
	<script>
		function reload() {
			$(".tg").load(" .tg > *");
		}

		function reload_queue() {
			$("#queue").load(" #queue > *");
		}

		function reload_files() {
			$("#files").load(" #files > *");
		}

		function reload_btn() {
			$("#button_table").load(location.href + " #button_table");
		}

		setInterval(function() {
			reload();
			reload_queue();
		}, 1000);

		setInterval(function() {
			oled()
		}, 5000);
	</script>

	<div id="status">
		<table class="tg">
			<tr>
				<td class="status_table">
					<?php
					$songInfo = $mpd->getCurrentSongInfo();
					if ($songInfo)
						echo "<img id='albumart' src=\"$songInfo[Artwork]\">";
					?>
				</td>
			</tr>
			<tr>
				<td class="status_table">
					<?php
					if ($songInfo) {
						if (strlen($songInfo['Name']) < 24)
							echo "<p class='info'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$songInfo[Name]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>";
						else
							echo "<p class='info'>$songInfo[Name]</p>";
						echo "<p class='info'>$songInfo[Album]</p>";
						echo "<p class='info'>$songInfo[Artist]</p>";
						echo "<p class='info'>$songInfo[Year]</p>";
						echo "<p class='info'>$songInfo[MinCurr]:$songInfo[SecCurr]"
						. " / $songInfo[MinLen]:$songInfo[SecLen]</p>";
					}
					?>
				</td>
			</tr>
		</table>
		<table id="button_table">
					<br>
					<button class="tooltip" type="button" onclick="prev();oled();reload();reload_queue();reload_btn();">
						<img class="media" src="icon/prev.webp">
						<span class="tooltiptext">Previous</span>
					</button>
					<?php
					$playingState = $mpd->getState();
					if ($playingState === "play")
						echo "<button class=\"tooltip\" type=\"button\"
						onclick=\"pause();reload();reload_btn();reload_queue();\">
						<img class=\"media\" src=\"icon/pause.webp\">
						<span class=\"tooltiptext\">Pause</span>
						</button>";
					elseif ($playingState === "pause")
						echo "<button class=\"tooltip\" type=\"button\"
						onclick=\"pause();reload();reload_btn();reload_queue();\">
						<img class=\"media\" src=\"icon/play.webp\">
						<span class=\"tooltiptext\">Play</span>
						</button>";
					else
						echo "<button class=\"tooltip\" type=\"button\"
						onclick=\"play();oled();reload();reload_btn();reload_queue();\">
						<img class=\"media\" src=\"icon/play.webp\">
						<span class=\"tooltiptext\">Play from beginning</span>
						</button>";
					?>
					<button class="tooltip" type="button" onclick="next();oled();reload();reload_queue();reload_btn();">
						<img class="media" src="icon/next.webp">
						<span class="tooltiptext">Next</span>
					</button>
					<button class="tooltip" type="button" onclick="seekb();">
						<img class="media" src="icon/seekb.webp">
						<span class="tooltiptext">Seek 10 seconds back</span>
					</button>
					<?php
					if ($playingState === "stop")
						echo "<button class=\"tooltip\" type=\"button\"
						onclick=\"reset();oled();reload();reload_btn();\">
						<img class=\"media\" src=\"icon/reset.webp\">
						<span class=\"tooltiptext\">Delete playlist</span>
						</button>";
					else
						echo "<button class=\"tooltip\" type=\"button\"
						onclick=\"stop();oled();reload();reload_queue();reload_btn();\">
						<img class=\"media\" src=\"icon/stop.webp\">
						<span class=\"tooltiptext\">Stop playing</span>
						</button>";
					?>
					<button class="tooltip" type="button" onclick="seekf();">
						<img class="media" src="icon/seekf.webp">
						<span class="tooltiptext">Seek 10 seconds forward</span>
					</button>
					<button class="tooltip" type="button" onclick="volup();">
						<img class="media" src="icon/volup.webp">
						<span class="tooltiptext">Increase volume by 10%</span>
					</button>
					<button class="tooltip" type="button" onclick="update();reload_files();">
						<img class="media" src="icon/update.webp">
						<span class="tooltiptext">Update MPD database</span>
					</button>
					<button class="tooltip" type="button" onclick="voldown();">
						<img class="media" src="icon/voldown.webp">
						<span class="tooltiptext">Lower volume by 10%</span>
					</button>
		</table>
	</div>

	<div id="tablica">
		<table id="song_playlist_table">
			<tr id="song_tr">
				<th id="t_head">
					<div id="files">
						<?php
						listArtists($mpd);
						if (isset($_POST['artist'])) listAlbums($_POST['artist'][0], $mpd);
						elseif (isset($_POST['album'])) listSongs($_POST['album'][0], $mpd);
						elseif (isset($_POST['upload'])) listUpload($mpd);
						elseif (isset($_POST['song'])) $mpd->playlistAdd($_POST['song'][0]);
						?>
					</div>

					<div id="queue">
						<?php
						listPlaylist($mpd);
						?>
					</div>
				</th>
			</tr>
		</table>
	</div>
</body>
</html>
