<!DOCTYPE html>
<html lang="hr">

<?php
require_once('php/connect.php');
require_once('php/Mobile_Detect.php');
?>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Raspberry Pi Jukebox</title>
	<link rel="icon" type="image/webp" href="icon/fav.webp" sizes="48x48">
	<link rel="stylesheet" href="css/index.css">
</head>

<body>
<div>
	<h1>Raspberry Pi Jukebox</h1>
	<?php

	if ($mpd->getConnectionStatus()) {
		echo "<h2 id='mpd_conn_y'>Uspješno povezan sa MPD ";
		echo $mpd->getVersion();
		echo " servisom!</h2>";
		if ((new Mobile_Detect)->isMobile()) echo "<button type=\"button\" onclick=\"location.href='mobile.php'; return false;\">></button>";
		else echo "<button type=\"button\" onclick=\"location.href='main.php'; return false;\">></button>";
	} else {
		echo "<h2 id='mpd_conn_n'>Neuspjelo povezivanje sa MPD servisom!</h2>";
		echo "<button type=\"button\" onclick=\"location.href='index.php'; return false;\">></button>";
	}
	?>
</div>
</body>
</html>
